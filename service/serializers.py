from rest_framework import serializers
from .models import Payments, Test

class PaymentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payments
        fields = ("delivery_amount", "destination", "ledger_index", "source",
                  "tx_hash", "transaction_cost", "source_tag", "destination_tag")

class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Test
        fields = ('id', 'company', 'amount')

