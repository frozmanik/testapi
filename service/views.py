from rest_framework import generics
from rest_framework.parsers import JSONParser
from rest_framework.utils import json

from .models import Payments
from django.views.decorators.csrf import csrf_exempt
from .Serializers.Request import *
from .Serializers.Message import *
from .Serializers.Account import *
from .Serializers.SalesPerson import *
from .serializers import PaymentsSerializer
from django.http import HttpResponse, JsonResponse
from service.Classes.MessageLog import MessageLog
from service.Classes.Account import Account
from service.Classes.SalesPerson import SalesPerson
import pyodbc
from datetime import datetime


# Create your views here.
credentials = ('Driver={ODBC Driver 17 for SQL Server};'
              'Server=sql6005.site4now.net;'
              'Database=eurodata;'
              'UID=eurodata_admin;'
              'pwd=qazplm123!'
               )


class ListRequestsView(generics.ListAPIView):
    """
    Provides a get method handler.
    """
    queryset = Requests.objects.all()
    serializer_class = RequestsSerializer

class ListPayments(generics.ListAPIView):
    queryset = Payments.objects.all()
    serializer_class = PaymentsSerializer

@csrf_exempt
def payment_list(request):
    if request.method == 'GET':
        payments = Payments.objects.all()
        serializer = PaymentsSerializer(payments, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = PaymentsSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = PaymentsSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def payment_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        snippet = Payments.objects.get(pk=pk)
    except Payments.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = PaymentsSerializer(snippet)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = PaymentsSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        snippet.delete()
        return HttpResponse(status=204)

@csrf_exempt
def request_detail(request, pk):
    """
    Retrieve, update or delete a code request.
    """
    try:
        req = Requests.objects.get(pk=pk)
    except Requests.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = RequestsSerializer(req)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = RequestsSerializer(req, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        req.delete()
        return HttpResponse(status=204)


@csrf_exempt
def getMessage(request, pk):
    try:
        mes = MessageLog(pk)
    except Exception as ex:
        return HttpResponse(status=404, content=str(ex))

    if request.method == 'GET':
        serializer = MessageSerializer(mes)
        return JsonResponse(serializer.data)

@csrf_exempt
def get_accounts(request,pk):
    '''
    get all accounts
    :param request:
    :return:
    '''
    if request.method == 'GET':
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()
            cursor.execute("""SELECT [PK_acc_chart_id]
                                   ,[FK_acc_chart_comp_id]
                                   ,[acc_chart_code]
                                   ,[acc_chart_name1]
                                   ,[FK_acc_chart_add_id] 
                                   FROM Accounting.acc_chart
                                   WHERE acc_chart_type = '320' and [FK_acc_chart_comp_id] in (SELECT [utl_comp_id]
                                      FROM [eurodata].[Utilities].[utl_usercomps]
                                      where utl_user_id = (?)) """, (pk))
            companies = cursor.fetchall()
            conn.close()

            accounts = []
            for row in companies:
                accounts.append(Account(row[0], row[1], row[2], row[3], row[4]))

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

        serializer = AccountSerializer(accounts, many=True)
        response = {'numberOfAccounts': len(accounts), 'accounts': serializer.data}
        return JsonResponse(response, safe=False, status=200)

@csrf_exempt
def get_companiesAccounts(request, pk1, pk2):
    '''
    get request for 1 account
    :param request:
    :param pk:
    :return:
    '''
    try:
        conn = pyodbc.connect(credentials)

        cursor = conn.cursor()
        cursor.execute("""SELECT [PK_acc_chart_id]
                        ,[FK_acc_chart_comp_id]
                        ,[acc_chart_code]
                        ,[acc_chart_name1]
                        ,[FK_acc_chart_add_id] 
                        FROM Accounting.acc_chart
                        WHERE acc_chart_type = '320' and [FK_acc_chart_comp_id] in (SELECT [utl_comp_id]
                            FROM [eurodata].[Utilities].[utl_usercomps]
                            where utl_user_id = (?) and FK_acc_chart_comp_id = (?)); """, (pk1, pk2,))

        rows = cursor.fetchall()
        conn.close()
        accounts = []
        for row in rows:
            accounts.append(Account(row[0], row[1], row[2], row[3], row[4]))

    except Exception as ex:
        return HttpResponse(status=404, content=str(ex))

    if request.method == 'GET':
        try:
            serializer = AccountSerializer(accounts, many=True)
            response = {'totalAccounts':len(accounts),'accounts':serializer.data}
            return JsonResponse(response, status=200, safe=False)
        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

@csrf_exempt
def get_salesPersons(request):
    if request.method == 'GET':
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()
            cursor.execute("""SELECT * FROM SOP.SalesPerson;""")
            data = cursor.fetchall()
            conn.close()
            salesPersons = []

            for row in data:
                salesPersons.append(SalesPerson(row[1], row[2], row[3], row[4],
                                                row[5], row[6], row[7], row[8], row[9],
                                                row[10], row[11], row[12], row[13]))

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

        serializer = SalesPersonSerializer(salesPersons, many=True)
        response = [{'numberOfSalesPersons': len(salesPersons), 'SalesPersons': serializer.data}]
        return JsonResponse(response, safe=False, status=200)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = SalesPersonSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

@csrf_exempt
def get_salesPerson(request, pk):
    if request.method == 'GET':
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()
            cursor.execute("""SELECT * FROM SOP.SalesPerson
                                WHERE ID = (?);""", (pk,))
            row= cursor.fetchone()
            conn.close()

            salePersone = SalesPerson(row[1], row[2], row[3], row[4],
                        row[5], row[6], row[7], row[8], row[9],
                        row[10], row[11], row[12], row[13])

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

        serializer = SalesPersonSerializer(salePersone)
        return JsonResponse(serializer.data, status=200)

    elif request.method == 'PUT':
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()
            query = """ UPDATE SOP.SalesPerson SET """
            parametrs = []
            data = JSONParser().parse(request)

            for key in data.keys():
                query = query + key + '= (?), '
                parametrs.append(data[key])

            parametrs.append(pk)
            query=query[:-2] + ' WHERE ID = (?);'
            cursor.execute(query,parametrs)
            cursor.commit()
            conn.close()
            return HttpResponse(status=200, content='Updated')

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

    elif request.method == 'DELETE':
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()
            cursor.execute("""DELETE SOP.SalesPerson WHERE ID = (?)""",(pk,))
            cursor.commit()
            conn.close()
            return HttpResponse(status=200, content='Deleted')

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

@csrf_exempt
def authorization(request):
    if request.method == 'GET':
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()
            data = JSONParser().parse(request)
            cursor.execute("""SELECT [PK_utl_user_id] FROM [Utilities].[utl_users]
                                        WHERE [utl_user_username] = (?) AND [utl_user_pass] = (?);""",
                           (data["login"], data["password"]))
            row = cursor.fetchone()
            if row != None:
                print(row)
                cursor.execute("""SELECT [utl_comp_id]
                                              FROM [eurodata].[Utilities].[utl_usercomps]
                                              where utl_user_id = (?) """, (row[0]))
                companies = cursor.fetchall()
                JSONComp = []
                conn.close()
                for id in companies:
                    JSONComp.append({'id': id[0]})

                access_token = {'access_token': 'test1234',
                                'id': row[0],
                                }
                return JsonResponse(access_token, status=200)
            else:
                response = {'answer': 'no such user'}
                return JsonResponse(response, status=500)

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

    elif request.method == 'POST':
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()
            data = JSONParser().parse(request)
            cursor.execute("""SELECT [PK_utl_user_id] FROM [Utilities].[utl_users]
                            WHERE [utl_user_username] = (?) AND [utl_user_pass] = (?);""", (data["login"], data["password"]))
            row = cursor.fetchone()
            if row != None:
                cursor.execute("""SELECT [utl_comp_id]
                                  FROM [eurodata].[Utilities].[utl_usercomps]
                                  where utl_user_id = (?) """,(row[0]))
                companies = cursor.fetchall()
                JSONComp = []
                conn.close()
                for id in companies:
                    JSONComp.append({'id':id[0]})

                access_token = {'access_token': 'test1234',
                                'id': row[0],
                                'companies':JSONComp}
                return JsonResponse(access_token, status=200)
            else:
                conn.close()
                response = {'answer': 'no such user'}
                return JsonResponse(response, status=500)

        except Exception as ex:
            conn.close()
            return HttpResponse(status=404, content=str(ex))

@csrf_exempt
def get_accountDetails(request, pk):
    if request.method == "GET":
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()

            address = cursor.execute("EXEC [Address].[add_addsp_AddressesReadByAddressBaseID] @address_base_id = ?", (pk,))
            addres = []
            if address.rowcount > 0:
                for row in address:
                    addres.append({"City": row[8],
                                   "Steet": row[3],
                                   "Area": row[4]
                                   })
            else:
                addres.append(({"City": "no data"}))

            emails = cursor.execute("EXEC [Address].[add_EmailReadByAddressID] @addres_base_id =  ?", (pk,))
            email = []
            if emails.rowcount > 0:
                for row in emails:
                    email.append({"email": row[3]})
            else:
                email.append({"email": "no data"})

            phones = cursor.execute("EXEC [Address].[add_phnsp_PhoneReadByAddressBaseID] @address_base_id = ?", (pk,))
            conn.close()
            phone = []

            if phones.rowcount > 0:
                for row in phones:
                    phone.append({"phone": row[3]})
            else:
                phone.append({"phone" : "no data"})

            result = {"address": addres,
                      "phones": phone,
                      "emails": email
                      }
            return JsonResponse(result, safe=False, status=200)

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

@csrf_exempt
def get_items(request):
    if request.method == "GET":
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()

            cursor.execute("SELECT [ID], [CompanyID], [Description1], [AverageCost] FROM [eurodata].[StockControl].[Item]")
            rows = cursor.fetchall()
            counter = len(rows)
            conn.close()
            items = []

            for row in rows:
                items.append({ 'id': row[0],
                               'companyID': row[1],
                               'description': row[2],
                               'price': row[3]})

            result = {'rows': counter,
                      'items': items}

            return JsonResponse(result, safe=False, status=200)

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

    elif request.method == "POST":
        try:
            conn = pyodbc.connect(credentials)
            data = JSONParser().parse(request)
            ids = data["ids"]
            cursor = conn.cursor()
            items = []
            count = 0
            print("EEEEE")
            for id in ids:
                cursor.execute('''SELECT  [ID],[CompanyID] ,[Code] ,[Description1] , [ShortDescription], [Type],[ItemStatus],[SalesStatus],[PurchasesStatus] ,[UnitOfMeasure1] ,[LastCost1]
                                                        FROM [eurodata].[StockControl].[Item]
                                                        WHERE ID = (?) ''', (id,))
                row = cursor.fetchone()
                items.append({'ID': row[0],
                          'CompanyID': row[1],
                          'Code': row[2],
                          'Description1': row[3],
                          'ShortDescription': row[4],
                          'Type': row[5],
                          'ItemStatus': row[6],
                          'SalesStatus': row[7],
                          'PurchasesStatus': row[8],
                          'UnitOfMeasure1': row[9],
                          'LastCost1': row[10]})
                count+=1

            result = {'itemCounter': count,
                      'ids': items}

            return JsonResponse(result, safe=False, status=200)

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

@csrf_exempt
def get_itemDetails(request,pk):
    if request.method == "GET":
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()

            cursor.execute('''SELECT  [ID],[CompanyID] ,[Code] ,[Description1] , [ShortDescription], [Type],[ItemStatus],[SalesStatus],[PurchasesStatus] ,[UnitOfMeasure1] ,[LastCost1]
                                        FROM [eurodata].[StockControl].[Item]
                                        WHERE ID = (?) ''', (pk,))
            row = cursor.fetchone()
            conn.close()
            result = {'ID':row[0],
                      'CompanyID':row[1],
                      'Code':row[2],
                      'Description1':row[3],
                      'ShortDescription':row[4],
                      'Type':row[5],
                      'ItemStatus':row[6],
                      'SalesStatus':row[7],
                      'PurchasesStatus':row[8],
                      'UnitOfMeasure1':row[9],
                      'LastCost1':row[10]}

            return JsonResponse(result, safe=False, status=200)

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

@csrf_exempt
def get_companies(request,pk):
    if request.method == 'GET':
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()
            cursor.execute("""SELECT [comp_id],[comp_name]
                              FROM [eurodata].[dbo].[sys_comp] 
                              WHERE comp_id in (SELECT utl_comp_id
                                FROM [eurodata].[Utilities].[utl_usercomps]
                                where utl_user_id = (?) ) """,(pk,))
            companies = cursor.fetchall()
            conn.close()
            data = []
            for row in companies:
                data.append({'name':row[1], 'id':row[0]})

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

        response = {'numberOfCompanies': len(data), 'companies': data}
        return JsonResponse(response, safe=False, status=200)

@csrf_exempt
def get_customerTransaction(request, pk1,pk2,pk3):
    if request.method == 'GET':
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()
            cursor.execute("""EXEC [SOP].[SummarisedDocumentReport]@UserID= ?,
                             @CompanyID= ?, @AccountIDs= ?, @OnlySaved=0,
                             @IncludeSaved=1 ,@IncludeCancelTransactions=0""", (pk1,pk2,pk3,))

            rows = cursor.fetchall()
            conn.close()
            data = []

            for i,row in enumerate(rows, 1):
                data.append({'id': i,
                        'documentNumber': row[3],
                        'documentDate': row[4],
                        'totalGross':row[8],
                        'documentTypeDescription':row[2],
                        'storeCode':row[25],
                        'storeDescription':row[26],
                        'userName':row[38],
                        'paymentMethod':row[6],
                        'documentDiscount':row[39]})

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

        response = {"numberOfTransaction": len(data), 'transactions':data}
        return JsonResponse( response, safe=False, status=200)

@csrf_exempt
def get_TransactionItems(request, pk1,pk2,pk3,inv):
    if request.method == 'GET':
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()

            if pk3 == 0:
                cursor.execute('''EXEC [SOP].AnalyticalDocumentReport @UserID= ?,
                                 @CompanyID= ?, @OnlySaved=0,
                                 @IncludeSaved=1 ,@IncludeCancelTransactions=0 , @fromdocumentnumber = ? , @todocumentnumber = ?''',(pk1,pk2,inv,inv,))

            else:
                cursor.execute('''EXEC [SOP].AnalyticalDocumentReport @UserID= ?,
                                             @CompanyID= ?, @AccountIDs= ?, @OnlySaved=0,
                                             @IncludeSaved=1 ,@IncludeCancelTransactions=0 , @fromdocumentnumber = ? , @todocumentnumber = ?''',(pk1, pk2, pk3, inv, inv,))
            rows=cursor.fetchall()
            conn.close()
            data=[]
            for row in rows:
                print(row)
                data.append({
                        'itemName': row[29],
                        'itemId': row[26],
                        'quantity':row[33],
                        })


        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

        response = {"numberOfTransaction": len(data), 'transactions':data}
        return JsonResponse( response, safe=False, status=200)

@csrf_exempt
def get_listOfTransactions(request, pk1,pk2):
    if request.method == 'GET':
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()
            cursor.execute("""EXEC [SOP].[SummarisedDocumentReport]@UserID= ?,
                             @CompanyID= ?, @OnlySaved=0,
                             @IncludeSaved=1 ,@IncludeCancelTransactions=0""", (pk1,pk2,))
            rows = cursor.fetchall()
            conn.close()
            data = []

            for i,row in enumerate(rows, 1):
                data.append({'id': i,
                        'documentNumber': row[3],
                        'documentDate': row[4],
                        'totalGross':row[8],
                        'documentTypeDescription':row[2],
                        'storeCode':row[25],
                        'storeDescription':row[26],
                        'userName':row[38],
                        'paymentMethod':row[6],
                        'documentDiscount':row[39]})

        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

        response = {"numberOfTransaction": len(data), 'transactions':data}
        return JsonResponse( response, safe=False, status=200)

@csrf_exempt
def create_order(request):
    if request.method == "POST":
        try:
            conn = pyodbc.connect(credentials)
            data = JSONParser().parse(request)
            cursor = conn.cursor()
            print(data)

            print('''exec [SOP].[OrderDocumentHeaders_CRUD] 
                @Status = {},
                @CompanyID={},
                @UserID={},
                @AccountID={},
                @DocumentType ={}, 
                @StoreID ={},
                @DocumentLines = {}, 
                @DocumentDate = {}, 
                @DueDate = {},
                @EntryDate = {},
                @ForeignAmount ={},
                @Quantity ={},
                @SalesValue = {},
                @CostValue = {},
                @VatAmount = {}, 
                @DocumentDiscountPercentage = {},
                @DocumentDiscount = {},
                @AverageLineDiscount = {},
                @WeeTax = {},
                @GreenTax = {}, 
                @AfisTax = {},
                @Year = {},
                @Period = {}, 
                @NetAmount = {},
                @TotalGross = {},
                @TotalDiscount = {},
                @UseOtherAddress = {}'''.format(data["Status"], data["CompanyID"], data["UserID"], data["AccountID"], data["DocumentType"],
                                                                                     data["StoreID"], data["DocumentLines"], data["DocumentDate"], data["DueDate"],
                                                                                     data["EntryDate"],data["ForeignAmount"], data["Quantity"], data["SalesValue"], data["CostValue"], data["VatAmount"],
                                                                                     data["DocumentDiscountPercentage"],data["DocumentDiscount"], data["AverageLineDiscount"], data["WeeTax"],
                                                                                     data["GreenTax"],data["AfisTax"], data["Year"], data["Period"], data["NetAmount"],data["TotalGross"],
                                                                                     data["TotalDiscount"], data["UseOtherAddress"]))
            cursor.execute("""exec [SOP].[OrderDocumentHeaders_CRUD] 
                            @Status = ?,
                            @CompanyID=?,
                            @UserID=?,
                            @AccountID=?,
                            @DocumentType =?, 
                            @StoreID =?,
                            @DocumentLines = ?, 
                            @DocumentDate = ?, 
                            @DueDate = ?,
                            @EntryDate = ?,
                            @ForeignAmount =?,
                            @Quantity =?,
                            @SalesValue = ?,
                            @CostValue = ?,
                            @VatAmount = ?, 
                            @DocumentDiscountPercentage = ?,
                            @DocumentDiscount = ?,
                            @AverageLineDiscount = ?,
                            @WeeTax = ?,
                            @GreenTax = ?, 
                            @AfisTax = ?,
                            @Year = ?,
                            @Period = ?, 
                            @NetAmount = ?,
                            @TotalGross = ?,
                            @TotalDiscount = ?,
                            @UseOtherAddress = ? ; """,(data["Status"], data["CompanyID"], data["UserID"], data["AccountID"], data["DocumentType"],
                                                                                     data["StoreID"], data["DocumentLines"], data["DocumentDate"], data["DueDate"],
                                                                                     data["EntryDate"],data["ForeignAmount"], data["Quantity"], data["SalesValue"], data["CostValue"], data["VatAmount"],
                                                                                     data["DocumentDiscountPercentage"],data["DocumentDiscount"], data["AverageLineDiscount"], data["WeeTax"],
                                                                                     data["GreenTax"],data["AfisTax"], data["Year"], data["Period"], data["NetAmount"],data["TotalGross"],
                                                                                     data["TotalDiscount"], data["UseOtherAddress"]))

            row = cursor.fetchone()
            cursor.commit()
            DocumentNumber = row[3]
            print(row)
            cursor.execute('''SELECT ID FROM SOP.OrderDocumentHeaders WHERE DocumentNumber = ? and DocumentType = ? and CompanyID = ? and UserID = ? and AccountID =? and StoreID = ?''',
                           (DocumentNumber, data["DocumentType"], data["CompanyID"],  data["UserID"], data["AccountID"], data["StoreID"] ))
            FKHeaderID = cursor.fetchone()[0]
            lineCount = 1
            print("HERE")
            for line in data["lines"]:

                cursor.execute('''SELECT [acc_vat_code]
                                  FROM [eurodata].[Accounting].[acc_vat]
                                  WHERE PK_acc_vat_id = (SELECT VAT1 FROM StockControl.Item WHERE ID = ? and CompanyID = ?)''', (line["FKItemID"],data["CompanyID"] ))

                vatCode = cursor.fetchone()[0]

                cursor.execute('''select acc_vat_rate_rate from accounting.acc_vat_rates where FK_acc_vat_comp_id = ? and FK_acc_vat_code = ?''',(data["CompanyID"], vatCode))
                vatRate = cursor.fetchone()[0]

                ''' probably somehow with god bless  we should use it to find price'''
                cursor.execute('''SELECT [ID] FROM [eurodata].[StockControl].[Contract] WHERE FKCompanyID = ? and FKItem = ? and FKPrice =10''', (data["CompanyID"], line["FKItemID"]))
                FKPriceID = cursor.fetchone()



                if FKPriceID is None:
                    FKPriceID = 21
                else:
                    FKPriceID = FKPriceID[0]

                # not correct way of finding price, used just to make it work and figured out problem later
                cursor.execute('''SELECT [FKPrice] FROM [eurodata].[StockControl].[ItemPrice] WHERE FKItem = ?''', (line["FKItemID"]))

                FKPriceID = cursor.fetchone()[0]

                cursor.execute('''EXEC [SOP].[OrderDocumentLines_CRUD]
                                @Status = ?,
                                @FKCompanyID = ?,
                                @FKHeaderID = ?,
                                @LineCount = ?,
                                @FKStoreID = ?,
                                @FKSalesPersonID = ?,
                                @FKItemID = ?,
                                @Quantity = ?,
                                @Pieces = ?,
                                @FKPriceID = ?,
                                @Price = ?,
                                @Amount = ?,
                                @PriceBeforeDiscount = ?,
                                @AmountBeforeDiscount = ?,
                                @DiscountPercentage = ?,
                                @DiscountAmount = ?,
                                @DiscountPercentageAmount = ?,
                                @DocumentDiscount = ?,
                                @Commission = ?,
                                @VatCode = ?,
                                @VatRate = ?,
                                @VatAmount = ?,
                                @WeeTax = ?,
                                @GreenTax = ?,
                                @AfisTax = ? ''',(data["Status"],  data["CompanyID"], FKHeaderID, lineCount, data["StoreID"], 51, line["FKItemID"],
                                                 line["Quantity"], 0, FKPriceID, line["Price"], line["Amount"], 0, 0, 0, 0, 0, 0, 0, str(vatCode), str(vatRate), str(float(line["Amount"]) * float(vatRate)), 0,0,0   ))
                lineCount+=1
                cursor.commit()
                print("222")
            result = {'header': FKHeaderID,
                      'DocumentNumber': DocumentNumber
                     }

            return JsonResponse(result, safe=False, status=200)

        except Exception as ex:
            print(str(ex))
            return HttpResponse(status=404, content=str(ex))

@csrf_exempt
def create_invoice(request):
    if request.method == "POST":
        try:
            conn = pyodbc.connect(credentials)
            data = JSONParser().parse(request)
            print(data)
            cursor = conn.cursor()
            cursor.execute('''SELECT [ID] ,[CompanyID],[UserID],[AccountID],[StoreID],[DocumentLines],[TenderDocumentHeaderID],[OrderDocumentHeaderID]
                             ,[WaybillDocumentHeaderID] ,[ProformaDocumentHeaderID],[PrintingCount],[DocumentDate],[DueDate] ,[EntryDate],[DocumentAddressID]
                             ,[DeliveryAddressID],[OtherAddress],[OtherTelephone],[OtherFax],[OtherEmail],[FKSalesPersonID]
                             ,[FKRouteID],[FKCollectorID],[FKDeliveryPerson],[Details],[DocumentStatus],[DocumentExported]
                             ,[IsMulticurrency],[ForeignAmount],[Rate],[PaymentMethod],[PaymentDetails],[Quantity]
                             ,[SalesValue],[CostValue],[VatInclude],[VatAmount],[DocumentDiscountPercentage],[DocumentDiscount]
                             ,[AverageLineDiscount],[AccountingSaveReference] ,[WeeTax] ,[GreenTax],[AfisTax]
                             ,[Year],[Period],[Time] ,[NetAmount] ,[TotalGross],[TotalDiscount] ,[UseOtherAddress]
                             ,[JournalID],[UpdatedDate],[JournalNumber],[MultiCurrencyCode],[ValidDate], [FKTenderStatus],[TenderAccountName] 
                             ,[OrderStatus],[FKSubAccountID],[UseVatExcempt],[VatCodeExcempt],[FKAnalysisID]
                                FROM [eurodata].[SOP].[OrderDocumentHeaders]
                                WHERE  UserID= ? and CompanyID= ? and AccountID= ? and DocumentNumber = ?''',(data['UserID'], data['CompanyID'], data['AccountID'], data['DocumentNumber']))

            row = cursor.fetchone()
            cursor.commit()
            CompanyID = row[1]
            UserID = row[2]
            AccountID = row[3]
            orderDocumentHeaderID = row[0]
            StoreID = row[4]
            DocumentLines = row[5]

            IsMulticurrency = row[27]
            ForeignAmount = row[28]
            Quantity = row[32]
            SalesValue = row[33]
            CostValue = row[34]
            VatInclude = row[35]
            VatAmount = row[36]
            DocumentDiscountPercentage = row[37]
            DocumentDiscount = row[38]
            AverageLineDiscount = row[39]

            AccountingSaveReference = row[40]
            WeeTax = row[41]
            GreenTax = row[42]
            AfisTax = row[43]
            Year = row[44]
            Period = row[45]
            NetAmount = row[47]
            TotalGross = row[48]
            TotalDiscount = row[49]
            UseOtherAddress = row[50]

            cursor.execute('''EXEC [SOP].[DocumentHeaders_CRUD]
                    @Status = N'Create',
                    @CompanyID = ?,
                    @DocumentType = ?,
                    @UserID = ?,
                    @AccountID = ?,
                    @OrderDocumentHeaderID = ?,
                    @StoreID = ?,
                    @DocumentLines = ?,
                    @DocumentDate =?,
                    @DueDate = ?,
                    @EntryDate = ?,
                    
                    @IsMulticurrency = ?,
                    @ForeignAmount = ?,
                    @Quantity = ?,
                    @SalesValue = ?,
                    @CostValue = ?,
                    @VatInclude = ?,
                    @VatAmount = ?,
                    @DocumentDiscountPercentage = ?,
                    @DocumentDiscount = ?,
                    @AverageLineDiscount = ?,
                    
                    @AccountingSaveReference =?,
                    @WeeTax = ?,
                    @GreenTax = ?,
                    @AfisTax = ?,
                    @Year = ?,
                    @Period = ?,
                    @NetAmount = ?,
                    @TotalGross = ?,
                    @TotalDiscount = ?,
                    @UseOtherAddress = ?,
                    
                    @SplitCash = ?,
                    @SplitCheque = ?,
                    @SplitVisa = ?''',(CompanyID, 15, UserID, AccountID, orderDocumentHeaderID, StoreID, DocumentLines, datetime.now(), datetime.now(), datetime.now(),
                                       IsMulticurrency, ForeignAmount, Quantity, SalesValue, CostValue, VatInclude, VatAmount, DocumentDiscountPercentage, DocumentDiscount, AverageLineDiscount,
                                       AccountingSaveReference, WeeTax, GreenTax, AfisTax, Year, Period, NetAmount, TotalGross, TotalDiscount, UseOtherAddress,
                                       0, 0, 0))

            result = cursor.fetchone()
            FKHeaderID = result[0]
            invoceNumber = result[3]
            cursor.commit()
            cursor.execute('''SELECT * FROM [eurodata].[SOP].[OrderDocumentLines]
                            WHERE FKHeaderID = ? and FKCompanyID = ? and FKStoreID = ?''',(orderDocumentHeaderID, data['CompanyID'], StoreID))

            orderLines = cursor.fetchall()

            # TODO: have lines from order now need to go throw each line to parse parameters to DocumentLines_CRUD

            for line in orderLines:
                cursor.execute('''EXEC [SOP].[DocumentLines_CRUD]
                            @Status = N'Create',
                            @FKCompanyID = ?,
                            @FKHeaderID = ?,
                            @LineCount = ?,
                            @FKStoreID = ?,
                            @FKSalesPersonID = ?,
                            @FKItemID = ?,
                            @Quantity = ?,
                            @Pieces = ?,
                            @FKPriceID = ?,
                            @Price = ?,
                            
                            @Amount = ?,
                            @PriceBeforeDiscount = ?,
                            @AmountBeforeDiscount = ?,
                            @DiscountPercentage = ?,
                            @DiscountAmount = ?,
                            @DiscountPercentageAmount = ?,
                            @DocumentDiscount = ?,
                            @Commission = ?,
                            @VatCode = ?,
                            @VatRate = ?,
                            
                            @VatAmount = ?,
                            @WeeTax = ?,
                            @GreenTax = ?,
                            @AfisTax = ?,
                            @Cost = ?''',(CompanyID, FKHeaderID, DocumentLines, StoreID, line[5], line[6], line[8], line[9], line[10], line[11],
                                          line[12], line[13], line[14], line[15], line[16], line[17], line[18], line[19], line[20], line[21],
                                          line[22], line[23], line[24], line[25], line[35]))
                cursor.commit()

            result = {'header': FKHeaderID,
                      'DocumentNumber': invoceNumber
                      }
            return JsonResponse(result, safe=False, status=200)
        except Exception as ex:
            return HttpResponse(status=404, content=str(ex))

@csrf_exempt
def create_receipt(request):
    if request.method == "POST":
        try:
            conn = pyodbc.connect(credentials)
            data = JSONParser().parse(request)
            print(data)
            cursor = conn.cursor()
            cursor.execute('''EXEC [SOP].[Receipt_CRUD]
                            @Status = N'Create',
                            @CompanyID = ?,
                            @AccountID = ?,
                            @UserID = ?,
                            @Description = ?,
                            @CurrencyCode = ?,
                            @StoreID = ?,
                            @ReceiptDate = ?,
                            
                            @EntryDate = ?,
                            @ReceiptRate = ?,
                            @ReceiptDiscount = ?,
                            @ReceiptAmount = ?,
                            @ReceiptTotal = ?,
                            @Details = ?,
                            @ReceiptStatus = ?,
                            
                            @PaymentType = ?,
                            @PrintCount = ?,
                            @IncludeSave = ?,
                            @BankCode = ?, 
                            @BankBranch = ?,
                            @ChequeNumber = ?,
                            @ChequeDate = ?''',(data["CompanyID"], data["AccountID"], data["UserID"], data["Description"], data["CurrencyCode"], data["StoreID"], datetime.now(),
                                                 datetime.now(), data["ReceiptRate"], data["ReceiptDiscount"], data["ReceiptAmount"], data["ReceiptTotal"], data["Details"], data["ReceiptStatus"],
                                                 data["PaymentType"], data["PrintCount"], data["IncludeSave"], data["BankCode"], data["BankBranch"], data["ChequeNumber"], data["ChequeDate"]))

            row = cursor.fetchone()
            print(row[8])
            cursor.commit()
            result = {'ID': row[0],
                      'DocumentNumber': row[8]
                      }
            return JsonResponse(result, safe=False, status=200)

        except Exception as ex:
            print(str(ex))
            return HttpResponse(status=404, content=str(ex))

def get_reaminingAmount(request,pk1):
    if request.method == "GET":
        try:
            conn = pyodbc.connect(credentials)
            cursor = conn.cursor()
            cursor.execute('''SELECT [opening_balance_both],[actual_both]
                              FROM [eurodata].[Accounting].[acc_balances]
                              WHERE account_id = ? and acc_balances_offset = (SELECT MAX(acc_balances_offset) FROM [eurodata].[Accounting].[acc_balances]
                              WHERE account_id = ?) ''', (pk1,pk1))

            row = cursor.fetchone()
            cursor.commit()
            result = {"RemainingAmount": row[0]+row[1]
                      }
            return JsonResponse(result, safe=False, status=200)

        except Exception as ex:
            print(str(ex))
            return HttpResponse(status=404, content=str(ex))







