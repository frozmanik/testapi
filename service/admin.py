from django.contrib import admin
from .models import Requests, Payments
from service.Classes.Account import *



# Register your models here.
admin.site.register(Requests)
admin.site.register(Payments)


#@admin.register(Test)
#class TestSummaryAdmin(admin.ModelAdmin):
#    change_list_template = 'admin/sale_summary_change_list.html'
#    date_hierarchy = 'created'
