from django.db import models

# Create your models here.
class Requests(models.Model):
    # id = models.IntegerField(label='ID', read_only=True)
    title = models.CharField(max_length=255, null=False)
    artist = models.CharField(max_length=255, null=False)

    filter_fields = ('title', 'artist')

    class Meta:
        ordering =('title',)

    def __str__(self):
        return "{} - {}".format( self.title, self.artist)


class Payments(models.Model):
    delivery_amount = models.FloatField(max_length=255, null=False)
    destination = models.CharField(max_length=255, null=False)
    ledger_index = models.IntegerField(max_length=255, null=False)
    source = models.CharField(max_length=255, null=False)
    tx_hash = models.CharField(max_length=255, null=False)
    transaction_cost = models.FloatField(max_length=255, null=False)
    source_tag = models.CharField(max_length=255, null=True, blank=True, default=None)
    destination_tag = models.CharField(max_length=255, null=True, blank=True, default=None)

    def __str__(self):
        return "{} : {} : {} : {} : {} : {} : {} : {}".format(self.delivery_amount, self.destination,
                                                           self.ledger_index, self.source, self.tx_hash,
                                                           self.transaction_cost, self.source_tag,
                                                           self.destination_tag)

class Test(models.Model):
    proxy = True
    company = models.CharField(max_length=255, null=False)
    amount = models.IntegerField(max_length=255, null=False)

    def __str__(self):
        return "{} : {}".format(self.company, self.amount)



