
class SalesPerson(object):
    def __init__(self, FKCompanyID, Code, Name, FKCommissionScale = None, FKAccountDue= None, FKAccountPayable= None,
                 FKGroups= None, Quota= None, FKStoreID= None, CommissionOveride1= None, CommissionOveride2= None,
                 CommissionOveride3= None,  CommissionOveride4= None, CommissionOveride5= None):
        self.FKCompanyID = FKCompanyID
        self.Code = Code
        self.Name = Name
        self.FKCommissionScale = FKCommissionScale
        self.FKAccountDue = FKAccountDue
        self.FKAccountPayable = FKAccountPayable
        self.FKGroups = FKGroups
        self.Quota = Quota
        self.FKStoreID = FKStoreID
        self.CommissionOveride1 = CommissionOveride1
        self.CommissionOveride2 = CommissionOveride2
        self.CommissionOveride3 = CommissionOveride3
        self.CommissionOveride4 = CommissionOveride4
        self.CommissionOveride5 = CommissionOveride5


    def __str__(self):
        return "{} {} {} {} {} {} {} \n {} {} {} {} {} {} {}".format(self.FKCompanyID, self.Code, self.Name, self.FKCommissionScale,
                                                                     self.FKAccountDue, self.FKAccountPayable, self.FKGroups,
                                                                     self.Quota, self.FKStoreID, self.CommissionOveride1,
                                                                     self.CommissionOveride2, self.CommissionOveride3,
                                                                     self.CommissionOveride4,self.CommissionOveride5)