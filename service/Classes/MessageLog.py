import pyodbc
from rest_framework import generics

class MessageLog(generics.ListAPIView):
    def __init__(self, Id):
        conn = pyodbc.connect('Driver={ODBC Driver 17 for SQL Server};'
                          'Server=sql6004.site4now.net;'
                          'Database=DB_A4A91D_test;'
                          'UID=DB_A4A91D_test_admin;'
                          'pwd=12123434Qq')
        cursor = conn.cursor()
        cursor.execute("""SELECT * FROM MessageLog WHERE ID= (?) """, (Id,) )
        data = cursor.fetchone()
        conn.close()
        self.Id = Id
        self.dateInit = data[1]
        self.logMessage = data[2]
        self.formName = data[3]
        self.success = data[4]


    def __str__(self):
        return "{} {} {} {} {}".format(self.Id, self.dateInit, self.logMessage, self.formName, self.success)

