from django.urls import path
from .views import *


urlpatterns = [
    path('requests/', ListRequestsView.as_view(), name="request-all"),
    path('requests/<int:pk>/', request_detail),
    path('payments/', payment_list),
    path('payments/<int:pk>/', payment_detail),
    path('message/<int:pk>/', getMessage),
    path('accounts/<int:pk>', get_accounts), # companies/salesPersonID/ (all accounts for sales persone)
    path('accounts/<int:pk1>/<int:pk2>/', get_companiesAccounts), # companies/salesPersonID/CompanyID
    path('account/<int:pk1>/<int:pk2>/<int:pk3>', get_customerTransaction),
    path('account/<int:pk1>/<int:pk2>/<int:pk3>/<slug:inv>', get_TransactionItems),
    path('account/details/<int:pk>/', get_accountDetails),
    path('account/<int:pk>/companies/',get_companies), # used when we input credentials to get list of companies
    path('transactions/<int:pk1>/<int:pk2>', get_listOfTransactions),
    path('salespersons/', get_salesPersons),
    path('salespersons/<int:pk>/', get_salesPerson),  # get, put
    path('auth/', authorization),
    path('items/', get_items),
    path('items/<slug:pk>', get_itemDetails),
    path('order/', create_order),
    path('invoice/', create_invoice),
    path('receipt/', create_receipt),
    path('receipt/<int:pk1>', get_reaminingAmount )

]