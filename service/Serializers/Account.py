from rest_framework import serializers

class AccountSerializer(serializers.Serializer):
    Pk_acc_chart_id = serializers.IntegerField()
    Fk_acc_chart_comp_id = serializers.IntegerField()
    acc_chart_code = serializers.CharField()
    acc_chart_name1 = serializers.CharField()
    FK_acc_chart_add_id = serializers.IntegerField()


class AccountsSerializer(serializers.Serializer):
    accounts = AccountSerializer()