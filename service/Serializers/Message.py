from rest_framework import serializers
from service.Classes.MessageLog import MessageLog


class MessageSerializer(serializers.Serializer):
    Id = serializers.IntegerField()
    dateInit = serializers.DateTimeField()
    logMessage = serializers.CharField()
    formName = serializers.CharField()
    success = serializers.BooleanField()

    def create(self, Id):
        return MessageLog(Id)

    def update(self, instance, validated_data):
        instance.Id = validated_data.get('Id', instance.Id)
        instance.dateInit = validated_data.get('dateInit', instance.dateInit)
        instance.logMessage = validated_data.get('logMessage', instance.logMessage)
        instance.formName = validated_data.get('formName', instance.formName)
        instance.success = validated_data.get('success', instance.success)
        instance.save()
        return instance
