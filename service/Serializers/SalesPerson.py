from rest_framework import serializers
from service.Classes.SalesPerson import SalesPerson
import pyodbc

class SalesPersonSerializer(serializers.Serializer):
    FKCompanyID = serializers.IntegerField()
    Code = serializers.CharField()
    Name = serializers.CharField()
    FKCommissionScale = serializers.IntegerField(allow_null=True)
    FKAccountDue = serializers.IntegerField(allow_null=True)
    FKAccountPayable = serializers.IntegerField(allow_null=True)
    FKGroups = serializers.IntegerField(allow_null=True)
    Quota = serializers.IntegerField(allow_null=True)
    FKStoreID = serializers.IntegerField(allow_null=True)
    CommissionOveride1 = serializers.CharField(allow_null=True)
    CommissionOveride2 = serializers.CharField(allow_null=True)
    CommissionOveride3 = serializers.CharField(allow_null=True)
    CommissionOveride4 = serializers.CharField(allow_null=True)
    CommissionOveride5 = serializers.CharField(allow_null=True)

    def create(self,FKCompanyID, Code, Name, FKCommissionScale = None, FKAccountDue= None, FKAccountPayable= None,
                 FKGroups= None, Quota= None, FKStoreID= None, CommissionOveride1= None, CommissionOveride2= None,
                 CommissionOveride3= None,  CommissionOveride4= None, CommissionOveride5= None):
        return SalesPerson(FKCompanyID, Code, Name, FKCommissionScale = None, FKAccountDue= None, FKAccountPayable= None,
                 FKGroups= None, Quota= None, FKStoreID= None, CommissionOveride1= None, CommissionOveride2= None,
                 CommissionOveride3= None,  CommissionOveride4= None, CommissionOveride5= None)

    def update(self, instance, validated_data):
        instance.FKCompanyID = validated_data.get('FKCompanyID',instance.FKCompanyID)
        instance.Code = validated_data.get('Code',instance.Code)
        instance.Name = validated_data.get('Name',instance.Name)
        instance.FKCommissionScale = validated_data.get('FKCommissionScale',instance.FKCommissionScale)
        instance.FKAccountDue = validated_data.get('FKAccountDue',instance.FKAccountDue)
        instance.FKAccountPayable = validated_data.get('FKAccountPayable',instance.FKAccountPayable)
        instance.FKGroups = validated_data.get('FKGroups',instance.FKGroups)
        instance.Quota = validated_data.get('Quota',instance.Quota)
        instance.FKStoreID = validated_data.get('FKStoreID',instance.FKStoreID)
        instance.CommissionOveride1 = validated_data.get('CommissionOveride1',instance.CommissionOveride1)
        instance.CommissionOveride2 = validated_data.get('CommissionOveride2',instance.CommissionOveride2)
        instance.CommissionOveride3 = validated_data.get('CommissionOveride3',instance.CommissionOveride3)
        instance.CommissionOveride4 = validated_data.get('CommissionOveride4',instance.CommissionOveride4)
        instance.CommissionOveride5 = validated_data.get('CommissionOveride5',instance.CommissionOveride5)
        return instance

    def save(self, **kwargs):
        conn = pyodbc.connect('Driver={ODBC Driver 17 for SQL Server};'
                              'Server=sql6004.site4now.net;'
                              'Database=DB_A4A91D_test;'
                              'UID=DB_A4A91D_test_admin;'
                              'pwd=12123434Qq'
                              )
        cursor = conn.cursor()
        cursor.execute('''INSERT INTO SOP.SalesPerson
                        VALUES((?),(?),(?),(?),(?),(?),(?),(?),(?),(?),(?),(?),(?),(?))''',
                       (self.data['FKCompanyID'], self.data['Code'], self.data['Name'], self.data['FKCommissionScale'],
                        self.data['FKAccountDue'], self.data['FKAccountPayable'], self.data['FKGroups'], self.data['Quota'],
                        self.data['FKStoreID'], self.data['CommissionOveride1'], self.data['CommissionOveride2'],
                        self.data['CommissionOveride3'], self.data['CommissionOveride4'], self.data['CommissionOveride5']))
        cursor.commit()
        conn.close()



