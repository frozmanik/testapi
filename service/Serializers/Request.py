from service.models import Requests
from rest_framework import serializers

class RequestsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Requests
        fields = ('id', 'title', 'artist')
